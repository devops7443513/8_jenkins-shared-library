#!/user/bin/env groovy

def call() {
    // enter app directory, because that's where package.json and tests are located
    dir("app") {
        // install all dependencies needed for running tests
        sh "npm install"
        sh "npm run test"
    }
}
