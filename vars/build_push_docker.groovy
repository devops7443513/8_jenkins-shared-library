#!/user/bin/env groovy

def call() {
    withCredentials([usernamePassword(credentialsId: 'docker-hub', usernameVariable: 'USER', passwordVariable: 'PASS')]){
        sh "docker build -t coolkartal/myapp:${IMAGE_NAME} ."
        sh 'echo $PASS | docker login -u $USER --password-stdin'
        sh "docker push coolkartal/myapp:${IMAGE_NAME}"
    }
}
