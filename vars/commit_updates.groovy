#!/user/bin/env groovy

def call() {
    withCredentials([usernamePassword(credentialsId: 'git-lab', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
        // git config here for the first time run
        sh 'git config --global user.email "jenkins@example.com"'
        sh 'git config --global user.name "jenkins"'
        sh 'git remote set-url origin https://$USER:$PASS@gitlab.com/devops7443513/8_jenkins-exercises.git'
        sh 'git add .'
        sh 'git commit -m "ci: version bump"'
        sh 'git push origin HEAD:jenkins-exercise'
    }
}
