#!/user/bin/env groovy

def call() {
    // enter app directory, because that's where package.json is located
    dir("app") {
        // update application version in the package.json file with one of these release types: patch, minor or major
        // this will commit the version update
        sh "npm version minor"

        // read the updated version from the package.json file
        def packageJson = readJSON file: 'package.json'
        def version = packageJson.version

        // set the new version as part of IMAGE_NAME
        env.IMAGE_NAME = "$version-$BUILD_NUMBER"
    }
}
